import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int min = 2147483647;
        int i;
        try (Scanner scanner = new Scanner(System.in)) {
            while ((i = scanner.nextInt()) != -1) {
                while (true) {
                    int mudNum = i % 10;
                    min = min > mudNum ? mudNum : min;
                    i = i / 10;
                    if (i == 0) break;
                }
            }
        }

        System.out.print(min);
    }
}
