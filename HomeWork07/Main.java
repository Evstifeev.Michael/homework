import java.util.Scanner;

/*На вход подается последовательность чисел, оканчивающихся на -1.
Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
Гарантируется:
Все числа в диапазоне от -100 до 100.
Числа встречаются не более 2 147 483 647-раз каждое.
Сложность алгоритма - O(n)
 */
public class Main {
    public static void main(String[] args) {
        int incomingDigits[] = new int[201];
        int i;
        try (Scanner scanner = new Scanner(System.in)) {
            while ((i = scanner.nextInt()) != -1) {
                if (i > 0 && i <= 100) {
                    incomingDigits[i] += 1;
                }
                if (i < 0 && i >= -100) {
                    i = -i + 100;
                    incomingDigits[i] += 1;
                }
            }
        }
        System.out.println(getMinDigit(incomingDigits));
    }

    static public int getMinDigit(int[] array) {
        int min = array[0];
        int bingo = 0;
        for (int i : array) {
            if (min == 0) {
                min = i;
            } else {
                min = ((min > i) && (i > 0)) ? i : min;
            }
        }

        for (int i = 0; i < array.length; i++) {
            if (array[i] == min) {
                bingo = i <= 100 ? i : (-i + 100);
            }
        }
        return bingo;
    }

}

/* порядок для проверки
7
8
8
9
9
7
77
77
7
-17
-18
-18
-1
*/