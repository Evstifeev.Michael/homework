

public class Main {
    public static void main(String[] args) {
        /**6.1 Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть индекс этого числа в массиве.
         * Если число в массиве отсутствует - вернуть -1.
         *Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например: */
        int tempArray[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        int tempNum = 7;
        int tempNum2 = 15;
        System.out.println(getIndexNum(tempArray, tempNum));
        System.out.println(getIndexNum(tempArray, tempNum2));


        /**6.2 Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
         было:
         34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
         стало
         34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
         */
        int tempArray2[] = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        /* Решение через 1 цикл */
        //semiBottleSort(tempArray2);
        /*Решение через 2 цикла */
        fullBottleSort(tempArray2);

        for(int i : tempArray2 ){
            System.out.print(i + " ");
        }
    }
    /*функция для 6.1  */
    static int getIndexNum(int[] eArray, int eNum) {
        for (int i = 0; i < eArray.length; i++) {
            if (eArray[i] == eNum) {
                return i;
            }
        }
        return -1;
    }

    /*6.2 Решение через 1 цикл (удовлетворяет условию, но не соответствует примеру)
    * Eсли значение передаваемого в массива(eArray[i]) равно нулю берёт, из конца значение (rightLine) и ставит на место нуля, а ноль на место rightLine.
    * После происходит декрементирование rightLine для установки следующего правого значения и i для проверки, не был ло ли в  rightLine нуля.
    * Алгоритм бегает по циклу пока i не станет равно rightLine.*/
    static void semiBottleSort(int[] eArray) {
        int rightLine = eArray.length - 1;
        for (int i = 0; i < eArray.length; i++) {
            if (eArray[i] == 0 && i <= rightLine) {
                int n = eArray[rightLine];
                eArray[rightLine] = eArray[i];
                eArray[i] = n;
                rightLine--;
                i--;
            }
        }

    }

    /*6.2 Решение через 2 цикла (удовлетворяет условию, соответствует примеру)
    * пузырьковая сортировка */
    static void fullBottleSort(int[] eArray) {
        for (int i = 0; i < eArray.length; i++) {
            for (int j = 0; j < eArray.length - 1; j++) {
                if (eArray[j] == 0) {
                    int tempNum = eArray[j];
                    eArray[j] = eArray[j + 1];
                    eArray[j + 1] = tempNum;
                }
            }
        }

    }

}
