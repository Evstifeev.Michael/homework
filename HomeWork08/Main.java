import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;

/*На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
Считать эти данные в массив объектов.
Вывести в отсортированном по возрастанию веса порядке.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        Human humans[] = new Human[10];

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int count = humans.length - 1;
            while (count >= 0) {
                String tempStrin = reader.readLine();
                String tempArray[] = tempStrin.split(", ");
                String name = tempArray[0];
                int weigh = Integer.parseInt(tempArray[1]);
                humans[count] = new Human(name, weigh);
                count--;
            }
        }
        System.out.println(Arrays.toString(humans));
        //Arrays.sort(humans, Comparator.comparing(human -> human.getWeigh()));
        botleSort(humans);
        System.out.print(Arrays.toString(humans));
    }

    public static class Human {
        private String name;
        private double weigh;

        public Human(String name, double weigh) {
            this.name = name;
            this.weigh = weigh;
        }

        public String getName() {
            return name;
        }

        public double getWeigh() {
            return weigh;
        }

        @Override
        public String toString() {
            return name + " " + weigh;
        }

    }

    public static void  botleSort(Human[] eArray){
        for (int i = 0; i < eArray.length; i++) {
            for (int j = 0; j < eArray.length - 1; j++) {
                if (eArray[j].getWeigh() > eArray[j+1].getWeigh()) {
                    Human tempNum = eArray[j];
                    eArray[j] = eArray[j + 1];
                    eArray[j + 1] = tempNum;
                }
            }
        }

    }
}
/*
asdsd, 10
as1dsd, 11
as2dsd, 12
as3dsd, 13
as4dsd, 14
as5dsd, 15
as6dsd, 16
as7dsd, 17
as8dsd, 18
as9dsd, 19
 */