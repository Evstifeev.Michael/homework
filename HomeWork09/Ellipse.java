package HomeWork09;

public class Ellipse extends Figure {

    final double pi = 3.14;

    public Ellipse() {
    }

    public Ellipse(int x, int y) {
        super(x, y);
    }

    public double getPerimeter() {
        return  4*(((pi * getX() * getY()) + Math.pow((getX()-getY()), 2)) / ((getX()+ getY())));




    }
}
