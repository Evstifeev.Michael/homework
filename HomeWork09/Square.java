package HomeWork09;

public class Square extends Rectangle{
    public Square(int x, int y) {
        super(x, y);
    }

    @Override
    public double getPerimeter() {
        return getX()*getY() ;
    }
}
