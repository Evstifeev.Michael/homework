package HomeWork09;
/*
Сделать класс Figure, у данного класса есть два поля - x и y координаты.
Классы Ellipse и Rectangle должны быть потомками класса Figure.
Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
В классе Figure предусмотреть метод getPerimeter(), который возвращает 0. Во всех остальных классах он должен возвращать корректное значение.

 */


public class Main {

    public static void main(String[] args) {
        int x = 4;
        int y = 7;

        System.out.println(new Figure(4, 7).getPerimeter());
        System.out.println(new Ellipse(4, 7).getPerimeter());
        System.out.println(new Circle(4, 7).getPerimeter());
        System.out.println(new Rectangle(4, 7).getPerimeter());
        System.out.println(new Square(4, 7).getPerimeter());


    }
}