package HomeWork09;

public class Circle extends Ellipse {
    public Circle(int x, int y) {
        super(x, y);
    }

    @Override
    public double getPerimeter() {
        return pi * Math.pow(getX(), 2);
    }
}
